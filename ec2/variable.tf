 variable "ami" {
    type = string
    description = "value of the ami"
 }
 variable "instance_type" {
    type = string
    description = "value of instance type"
 }
 variable "subnet_id" {
    type = string
   description = "value of the subnet_id"
 }